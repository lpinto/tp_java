package org.ldv.sio;

public class AdresseEtendue {
    private String mail;
    private String url;

    public AdresseEtendue(String mail, String url) {
        this.mail = mail;
        this.url = url;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    @Override
    public String toString() {
        return " mail = " + mail +
                ", url = " + url
                ;
    }
}
